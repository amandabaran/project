extern crate nom;
extern crate cse262_project;

use cse262_project::program;

fn main() {
  let result = program("let x = 1 + 1;");
  println!("{:?}", result);
}
